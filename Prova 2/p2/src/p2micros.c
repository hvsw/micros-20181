/*
 * Leonardo Dorneles e Thomas Fontanari
 */

#include <math.h>
#include <byteswap.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h>
#include <stdint.h>

#include "libgalileo2.h"
#include <jhdlcd.h>
#include <i2cutil.h>
#include <sys/ioctl.h>

#include <linux/i2c-dev.h>

#define DATA_POINTS 100
#define SAMPLING_PERIOD 1e-3


/* Assumes the data format for Galileo Gen2 */
struct sensors
{
        uint16_t pot;		/* be:u12/16>>0 */
        int64_t timestamp;	/* le:s64/64>>0 */
};

static volatile int run=1;

float avg;
float std;

static int degrees;
static float radians;

void quit(int signal)
{
    run=0;
}

int test_pwm_avg(int dc)
{
    PWM *pwm = gpwm_open(1);
    
    // int i = 0;
    gpwm_write_dc_percent(pwm, 0);

    gpwm_enable(pwm);

    gpwm_write_dc_percent(pwm, dc);
    usleep(1000000); // 15 seconds

    gpwm_close(pwm);
    return 0;
}

int set_servo_pos(int degrees, PWM *pwm)
{
    int duty_cycle;
    float angle = degrees * M_PI/180.0;
    
    
    duty_cycle = angle/M_PI_2*850000+1500000;
    
    // printf("dc: %d, angle: %f degrees: %d\n", duty_cycle, angle, degrees);
    
    if(gpwm_write_duty_cycle(pwm, duty_cycle) <0 ) {
      printf("Erro write duty cycle\n");
      return -1;
    }
      
    
    return 0;
}

int read_pot()
{
  char data_str[80];
  struct sensors data[100];
  float scale;
  int samples;
  int fd;
  float pot;
  int i;

  pputs("/sys/bus/iio/devices/iio:device0/buffer/enable","0");
  pgets(data_str, sizeof data_str, "/sys/bus/iio/devices/iio:device0/in_voltage0_scale");
  scale = atof(data_str)/1000.0;
  
  pputs("/sys/bus/iio/devices/iio:device0/scan_elements/in_voltage0_en", "1");
  
  pputs("/sys/bus/iio/devices/iio:device0/scan_elements/in_timestamp_en", "1");
  snprintf(data_str,sizeof data_str,"%d", 100);
  pputs("/sys/bus/iio/devices/iio:device0/buffer/length", data_str);
  
  
  pgets(data_str, sizeof data_str, "/sys/bus/iio/devices/trigger0/name");
  pputs("/sys/bus/iio/devices/iio:device0/trigger/current_trigger",data_str);
  
  snprintf(data_str,sizeof data_str,"%d", 1000);
  pputs("/sys/bus/iio/devices/trigger0/frequency", data_str);
  
  pputs("/sys/bus/iio/devices/iio:device0/buffer/enable","1");
  
  //usleep(100000);
  sleep(ceil(DATA_POINTS * SAMPLING_PERIOD));
  
  pputs("/sys/bus/iio/devices/iio:device0/buffer/enable","0");
  
  pputs("/sys/bus/iio/devices/iio:device0/trigger/current_trigger","\n");

  
  if((fd = open("/dev/iio:device0",O_RDONLY)) < 0)
  {
    perror("Opening /dev/iio:device0:");
    return -1;
  }
  
  
  samples = read(fd,data,sizeof data)/sizeof(struct sensors);
  close(fd);
  printf("SAMPLES READ: %d\n", samples);
  
  pputs("/sys/bus/iio/devices/iio:device0/buffer/length","2");
  
  pputs("/sys/bus/iio/devices/iio:device0/scan_elements/in_voltage0_en","0");
  pputs("/sys/bus/iio/devices/iio:device0/scan_elements/in_timestamp_en","0");
  
  
  //printf("scale: %f\n", scale);
  avg = 0;
  for(i=0;i < samples; i++) {
    pot = bswap_16(data[0].pot) * scale;
    avg += pot;
    //printf("Pot: %f\n", pot);
  }
  avg = avg/samples;
  
  
  std = 0;
  for(i=0;i < samples;i++) {
    pot = bswap_16(data[0].pot) * scale;
    std += (pot - avg) * (pot - avg);
  }
  std = std/samples;
  
  
  return 0;
}


int init_lcd(){
    int fd;
    if((fd=open("/dev/i2c-0",O_WRONLY)) < 0) printf("Opening /dev/i2c-0");
    usleep(30000);	/* Wait for 30 ms after power on */

    if(ioctl(fd,I2C_SLAVE,LCD_ADDR) < 0) printf("ioctl on /dev/i2c-0");

    i2c_write_reg(fd,LCD_C0,LCD_FUNCTIONSET | LCD_2LINE);
    usleep(40);  /* Wait for more than 39 us */

    i2c_write_reg(fd,LCD_C0,LCD_DISPLAYSWITCH | LCD_DISPLAYON | 
	    LCD_CURSOROFF | LCD_BLINKOFF);
    usleep(40);	/* Wait for more then 39 us */

    i2c_write_reg(fd,LCD_C0,LCD_SCREENCLEAR);
    usleep(1600);	/* Wait for more then 1.53 ms */
    
    i2c_write_reg(fd,LCD_C0,LCD_INPUTSET | LCD_ENTRYLEFT | LCD_DECREMENT);

    return fd;
}

int write_lcd(int fd, int line){
  int i;
  if(ioctl(fd,I2C_SLAVE,LCD_ADDR) < 0) printf("ioctl on /dev/i2c-0");
  char str[16] = "                ";
 
  
  switch(line){
    case LINE_1 : 
	i2c_write_reg(fd,LCD_C0,LCD_DDRAMADDRSET | 0);
	snprintf(str, sizeof str, "%.2f, %0.2f", avg, std);
	for(i=0;i < 16; i++) i2c_write_reg(fd,LCD_RS, str[i]);
	break;
    case LINE_2:
        i2c_write_reg(fd,LCD_C0,LCD_DDRAMADDRSET | 0x40);
	snprintf(str, sizeof str, "%d, %f", degrees, radians);
	for(i=0;i < 16; i++) i2c_write_reg(fd,LCD_RS, str[i]);
  }
                
  return 0;
}

int main(int argc, char *argv[])
{
    int lcd_fd = init_lcd();
    
    
    usleep(1000000);
    while(1) {
      
      read_pot();
      
      PWM *pwm = gpwm_open(1);
      
      
      

      gpwm_write_dc_percent(pwm, 0);
      
      gpwm_enable(pwm);
    
      degrees = 36*avg - 90;
      radians = degrees * M_PI / 180;
      set_servo_pos(degrees, pwm);
      
    
      write_lcd(lcd_fd, LINE_1);
      write_lcd(lcd_fd, LINE_2);
      
      usleep(1000000);
      gpwm_close(pwm);
    }
    
    
    

    return 0;
}