#ifndef GPWM_H
#define GPWM_H

#define PWM_MAX_PERIOD 41600000
#define PWM_MIN_PERIOD 656000
#define PWM_DEFAULT_PERIOD 20000000
#define GPIO_HIGH 1
#define GPIO_LOW 0

#define READ_ONLY 0
#define WRITE_ONLY 1
#define READ_WRITE 2

#define LINE_2 2
#define LINE_1 1

typedef struct st_gpio {
    int fd;
    int mode;
} GPIO;

typedef struct st_pwm {
    int fd_enable;
    int fd_duty_cycle;
} PWM;

typedef struct st_adcraw {
    int fd_vraw;
    int fd_scale;
    int channel;
} ADC1S;

// ADC1S LIBRARY
ADC1S *gadc_open(int channel);
float gadc_read_scale(ADC1S *adc); // mV
int gadc_read_volt_raw(ADC1S *adc);
float gadc_read_voltage(ADC1S *adc); // mV
int gadc_close(ADC1S *adc);

// PWM LIBRARY
PWM *gpwm_open(int pwm);
int gpwm_write_duty_cycle(PWM *pwm, int duty_cycle);
int gpwm_write_dc_percent(PWM *pwm, int percent);
int gpwm_write_pwm_period(int pwm_period);
int gpwm_enable(PWM *pwm);
int gpwm_disable(PWM *pwm);
int gpwm_close(PWM *pwm);

// GPIO LIBRARY
GPIO *ggpio_open(char *file, int mode);
int ggpio_write(GPIO *gpio, int value);
int ggpio_read(GPIO *gpio);
int ggpio_close(GPIO *gpio);

// FUNÇÕES DE MICROCONTROLADORES
char *pgets(char *s, int size, const char path[]);
int pputs(const char path[], const char s[]);

#endif