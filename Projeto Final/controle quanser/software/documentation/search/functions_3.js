var searchData=
[
  ['getlastsample',['getLastSample',['../pid_8h.html#a898c72edb11d63459edc5791597ee99b',1,'pid.h']]],
  ['getnewsample',['getNewSample',['../pid_8h.html#a14bf79c4c369230ce81498ba9da0415a',1,'pid.h']]],
  ['gettimeat',['getTimeAt',['../pid_8h.html#ac7c53736fa09ef11ca92f963c3b2070c',1,'pid.h']]],
  ['ggpio_5fclose',['ggpio_close',['../ggpio_8h.html#ae41876b68ebe34bbe73e30a55c2cfc88',1,'ggpio.h']]],
  ['ggpio_5fopen',['ggpio_open',['../ggpio_8h.html#a9410650e310bded8609538f0bbe168d5',1,'ggpio.h']]],
  ['ggpio_5fread',['ggpio_read',['../ggpio_8h.html#a50fe00263b2edc16c86cca8440f15a70',1,'ggpio.h']]],
  ['ggpio_5fwrite',['ggpio_write',['../ggpio_8h.html#a7c8cddafdc35fafbffbf9736345d7581',1,'ggpio.h']]],
  ['gpwm_5fclose',['gpwm_close',['../gpwm_8h.html#ac71ca0cab61d469edebcf88613bed452',1,'gpwm.h']]],
  ['gpwm_5fdisable',['gpwm_disable',['../gpwm_8h.html#a80ce27b9ebf9bbb4bfdf9cc481d92487',1,'gpwm.h']]],
  ['gpwm_5fenable',['gpwm_enable',['../gpwm_8h.html#ab988efdd989e8518c240e3a0465b1ecf',1,'gpwm.h']]],
  ['gpwm_5fopen',['gpwm_open',['../gpwm_8h.html#af866b5fc1b92c78c346cf51faf3d3b05',1,'gpwm.h']]],
  ['gpwm_5fwrite_5fdc_5fpercent',['gpwm_write_dc_percent',['../gpwm_8h.html#ae32854919c25157453af6b9318c9e86a',1,'gpwm.h']]],
  ['gpwm_5fwrite_5fduty_5fcycle',['gpwm_write_duty_cycle',['../gpwm_8h.html#afb2b9a187687caedc1c06027fdd90351',1,'gpwm.h']]],
  ['gpwm_5fwrite_5fpwm_5fperiod',['gpwm_write_pwm_period',['../gpwm_8h.html#a465f0a7b6f0ad8b2f34a8f9b8bd4d02f',1,'gpwm.h']]]
];
