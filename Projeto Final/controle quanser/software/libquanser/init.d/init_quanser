#! /bin/sh
### BEGIN INIT INFO
# Provides:          init_quanser
# Required-Start:    
# Should-Start:      
# Required-Stop:     
# Default-Start:     S
# Default-Stop:
# Short-Description: Configures gpios, pwm and spi bus for use with libquanser.
# Description:       Configures gpios, pwm and spi bus for use with libquanser.
### END INIT INFO

case "$1" in
    start|restart|force-reload)

	## CONFIGURES IO5 AS PWM3 TO CONTROL MOTOR
	if [ ! -d /sys/class/pwm/pwmchip0/pwm3 ] ; then
	    echo -n "3" > /sys/class/pwm/pwmchip0/export
	fi
	chgrp qnsr /sys/class/pwm/pwmchip0/device/pwm_period
	chmod g+w /sys/class/pwm/pwmchip0/device/pwm_period
	chgrp qnsr /sys/class/pwm/pwmchip0/pwm3/duty_cycle
	chmod g+w /sys/class/pwm/pwmchip0/pwm3/duty_cycle
	chgrp qnsr /sys/class/pwm/pwmchip0/pwm3/enable
	chmod g+w /sys/class/pwm/pwmchip0/pwm3/enable
        
    # gpio18 = 0 = out
	if [ ! -d /sys/class/gpio/gpio18 ] ; then
	    echo -n "18" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio18/direction
	echo -n "0" > /sys/class/gpio/gpio18/value

    # gpio19 = in = no pull-up nor pull-down
	if [ ! -d /sys/class/gpio/gpio19 ] ; then
	    echo -n "19" > /sys/class/gpio/export
	fi
	echo -n "in" > /sys/class/gpio/gpio19/direction

    # gpio66 = 1 */
	if [ ! -d /sys/class/gpio/gpio66 ] ; then
	    echo -n "66" > /sys/class/gpio/export
	fi
	echo -n "1" > /sys/class/gpio/gpio66/value

    ## CONFIGURES GPIOS FOR SWITCH 2 IN IO4
    if [ ! -d /sys/class/gpio/gpio6 ] ; then
	    echo -n "6" > /sys/class/gpio/export
	fi
	echo -n "in" > /sys/class/gpio/gpio6/direction
	chgrp qnsr /sys/class/gpio/gpio6/value
    chmod g+rw /sys/class/gpio/gpio6/value
	
	if [ ! -d /sys/class/gpio/gpio36 ] ; then
	    echo -n "36" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio36/direction
	echo -n "1" > /sys/class/gpio/gpio36/value

	if [ ! -d /sys/class/gpio/gpio37 ] ; then
	    echo -n "37" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio37/direction
	echo -n "0" > /sys/class/gpio/gpio37/value

    ## CONFIGURES GPIO FOR SWITCH 1 IN IO3 
    if [ ! -d /sys/class/gpio/gpio14 ] ; then
	    echo -n "14" > /sys/class/gpio/export
	fi
	echo -n "in" > /sys/class/gpio/gpio14/direction
	chgrp qnsr /sys/class/gpio/gpio14/value
    chmod g+rw /sys/class/gpio/gpio14/value
	
	if [ ! -d /sys/class/gpio/gpio16 ] ; then
	    echo -n "16" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio16/direction
	echo -n "1" > /sys/class/gpio/gpio16/value

	if [ ! -d /sys/class/gpio/gpio17 ] ; then
	    echo -n "17" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio17/direction
	echo -n "0" > /sys/class/gpio/gpio17/value

	if [ ! -d /sys/class/gpio/gpio76 ] ; then
	    echo -n "76" > /sys/class/gpio/export
	fi
	echo -n "0" > /sys/class/gpio/gpio76/value

	if [ ! -d /sys/class/gpio/gpio64 ] ; then
	    echo -n "64" > /sys/class/gpio/export
	fi
	echo -n "0" > /sys/class/gpio/gpio64/value

    ## CONFIGURES GPIO FOR ENABLING H-BRIDGE IN IO6
    if [ ! -d /sys/class/gpio/gpio1 ] ; then
	    echo -n "1" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio1/direction
	echo -n "0" > /sys/class/gpio/gpio1/value
    chgrp qnsr /sys/class/gpio/gpio1/value
    chmod g+rw /sys/class/gpio/gpio1/value
	
	if [ ! -d /sys/class/gpio/gpio20 ] ; then
	    echo -n "20" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio20/direction
	echo -n "0" > /sys/class/gpio/gpio20/value

	if [ ! -d /sys/class/gpio/gpio68 ] ; then
	    echo -n "68" > /sys/class/gpio/export
	fi
	echo -n "0" > /sys/class/gpio/gpio68/value

    ## CONFIGURES GPIO FOR ENABLING DECODER IN IO8
    if [ ! -d /sys/class/gpio/gpio40 ] ; then
	    echo -n "40" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio40/direction
    echo -n "0" > /sys/class/gpio/gpio40/value
	chgrp qnsr /sys/class/gpio/gpio40/value
    chmod g+rw /sys/class/gpio/gpio40/value

    ## CONFIGURES SERIAL INTERFACE WITH LS7366R
	# Configures IO10 for SPI #SS
	#	gpio26=0 (output)
	#	gpio27=1 (pull-up)
	#	gpio74=0 (IO10 is GPIO)
	if [ ! -d /sys/class/gpio/gpio26 ] ; then
	    echo -n "26" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio26/direction
	echo -n "0" > /sys/class/gpio/gpio26/value

	if [ ! -d /sys/class/gpio/gpio27 ] ; then
	    echo -n "27" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio27/direction
	echo -n "1" > /sys/class/gpio/gpio27/value
	
	if [ ! -d /sys/class/gpio/gpio74 ] ; then
	    echo -n "74" > /sys/class/gpio/export
	fi
	echo -n "0" > /sys/class/gpio/gpio74/value

	if [ ! -d /sys/class/gpio/gpio10 ] ; then
	    echo -n "10" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio10/direction

	chgrp qnsr /sys/class/gpio/gpio10/value
    chmod g+rw /sys/class/gpio/gpio10/value
	
	# Configures IO11 for SPI MOSI
	#	gpio24=0 (output)
	#	gpio25=input (floating)
	#	gpio44=1 (IO11 is SPI)
	#	gpio72=0 (IO11 is SPI)
	if [ ! -d /sys/class/gpio/gpio24 ] ; then
	    echo -n "24" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio24/direction
	echo -n "0" > /sys/class/gpio/gpio24/value
	
	if [ ! -d /sys/class/gpio/gpio25 ] ; then
	    echo -n "25" > /sys/class/gpio/export
	fi
	echo -n "in" > /sys/class/gpio/gpio25/direction
	
	if [ ! -d /sys/class/gpio/gpio44 ] ; then
	    echo -n "44" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio44/direction
	echo -n "1" > /sys/class/gpio/gpio44/value
	
	if [ ! -d /sys/class/gpio/gpio72 ] ; then
	    echo -n "72" > /sys/class/gpio/export
	fi
	echo -n "0" > /sys/class/gpio/gpio72/value

	# Configures IO12 for SPI MISO
	#	gpio42=1 (input)
	#	gpio43=input (floating)
	if [ ! -d /sys/class/gpio/gpio42 ] ; then
	    echo -n "42" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio42/direction
	echo -n "1" > /sys/class/gpio/gpio42/value
	
	if [ ! -d /sys/class/gpio/gpio43 ] ; then
	    echo -n "43" > /sys/class/gpio/export
	fi
	echo -n "in" > /sys/class/gpio/gpio43/direction

	# Configure IO13 for SPI SCLK
	#	gpio30=0 (output)
	#	gpio31=0 (pull-down, SCLK mode 0)
	#	gpio46=1 (IO13 is SPI)
	if [ ! -d /sys/class/gpio/gpio30 ] ; then
	    echo -n "30" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio30/direction
	echo -n "0" > /sys/class/gpio/gpio30/value
	
	if [ ! -d /sys/class/gpio/gpio31 ] ; then
	    echo -n "31" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio31/direction
	echo -n "0" > /sys/class/gpio/gpio31/value
	
	if [ ! -d /sys/class/gpio/gpio46 ] ; then
	    echo -n "46" > /sys/class/gpio/export
	fi
	echo -n "out" > /sys/class/gpio/gpio46/direction
	echo -n "1" > /sys/class/gpio/gpio46/value

    chgrp qnsr /dev/spidev1.0
    chmod g+rw /dev/spidev1.0
    	;;
    stop)

	## UNEXPORT GPIOs AND PWM FOR PWM3 IN IO5
    echo -n "0" > /sys/class/pwm/pwmchip0/pwm3/enable
    echo -n "66" > /sys/class/gpio/unexport
	echo -n "19" > /sys/class/gpio/unexport
	echo -n "1" > /sys/class/gpio/gpio18/value
	echo -n "18" > /sys/class/gpio/unexport
	echo -n "3" > /sys/class/pwm/pwmchip0/unexport

    ## UNEXPORT GPIOS RESPONSIBLE FOR SWITCH 2
    echo -n "in" > /sys/class/gpio/gpio6/direction
    echo -n "6" > /sys/class/gpio/unexport
	echo -n "36" > /sys/class/gpio/unexport
	echo -n "37" > /sys/class/gpio/unexport

    ## UNEXPORT GPIOS RESPONSIBLE FOR SWITCH 1
    echo -n "in" > /sys/class/gpio/gpio14/direction
    echo -n "14" > /sys/class/gpio/unexport
	echo -n "16" > /sys/class/gpio/unexport
	echo -n "17" > /sys/class/gpio/unexport
    echo -n "76" > /sys/class/gpio/unexport
    echo -n "64" > /sys/class/gpio/unexport

    ## STOP GPIO RESPONBISBLE FOR H-BRIDGE ENABLE
    echo -n "in" > /sys/class/gpio/gpio1/direction
    echo -n "1" > /sys/class/gpio/unexport
	echo -n "20" > /sys/class/gpio/unexport
	echo -n "68" > /sys/class/gpio/unexport

    ## STOP GPIO RESPONSIBLE FOR DECODER ENABLE
    echo -n "in" > /sys/class/gpio/gpio40/direction
    echo -n "40" > /sys/class/gpio/unexport

    ## STOP LS7366R
	echo -n "1" > /sys/class/gpio/gpio26/value
	echo -n "26" > /sys/class/gpio/unexport
	echo -n "in" > /sys/class/gpio/gpio27/direction
	echo -n "74" > /sys/class/gpio/unexport
	echo -n "1" > /sys/class/gpio/gpio24/value
	echo -n "24" > /sys/class/gpio/unexport
	echo -n "25" > /sys/class/gpio/unexport
	echo -n "0" > /sys/class/gpio/gpio44/value
	echo -n "44" > /sys/class/gpio/unexport
	echo -n "72" > /sys/class/gpio/unexport
	echo -n "10" > /sys/class/gpio/unexport
	echo -n "42" > /sys/class/gpio/unexport
	echo -n "43" > /sys/class/gpio/unexport
	echo -n "0" > /sys/class/gpio/gpio46/value
	echo -n "46" > /sys/class/gpio/unexport
	echo -n "in" > /sys/class/gpio/gpio31/direction
	echo -n "31" > /sys/class/gpio/unexport
	echo -n "1" > /sys/class/gpio/gpio30/value
	echo -n "30" > /sys/class/gpio/unexport
    ##

	;;
    status)
	;;
    *)
	echo -n "Usage: $0 " 
	echo "{start|stop|restart|force-reload|status}"
	exit 1
	;;
esac

exit 0
