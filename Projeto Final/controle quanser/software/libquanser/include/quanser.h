/*
 * Libary containing methods to control Quanser 2DSJF using shield given
 * in this work.
 */
#ifndef QUANSER_H
#define QUANSER_H

#include <ggpio.h>
#include <gpwm.h>
#include <ls7366r.h>

#define GPIO_DEC_EN 40
#define GPIO_MOTOR_EN 1
#define GPIO_SW1 14
#define GPIO_SW2 6
#define PWM_MOTOR 3

#define SWITCH1 1
#define SWITCH2 2

typedef struct {
    GPIO decoder_en; // IO8 gpio40
    DECODER decoder;
    GPIO motor_en; // IO6 gpio1
    PWM motor;
    GPIO switch1; // IO3 gpio14
    GPIO switch2; // IO4 gpio6
} QUANSER;

/**
    Open descriptors to use libquanser.
    Return 0 if success and -1 otherwise.
**/
int quanser_open(QUANSER *quanser);

/**
    Enable or disable motor described in _quanser_.
    Controls both PWM enable signal and GPIO signal controlling L6203.
    Return 0 if success and -1 otherwise.
**/
int quanser_enable_motor(QUANSER quanser, int enable);

/**
    Set voltage _float_ to motor in _quanser.
    Return 0 if success and -1 otherwise.
**/
int quanser_set_motor_volts(QUANSER quanser, float volts);

/**
    Enable or disable decoder described in _quanser_.
    Controls GPIO used to enable or disable LS7366R.
**/
int quanser_enable_decoder(QUANSER quanser, int enable);

/**
    Read value of encoder in radians.
    Return value read in radians.
**/
float quanser_read_encoder_rad(QUANSER quanser);

/**
    Read value of switch _which_ in _quanser. _which_ can be
    SWITCH1 or SWITCH2.
    Return value read.
**/
int quanser_read_switch(QUANSER quanser, int which);

/**
    Close structures in _quanser_.
    Return 0 if success and -1 otherwise.
**/
int quanser_close(QUANSER quanser);

#endif
