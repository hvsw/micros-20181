#ifndef PID
#define PID
#define ERROR 0.001
#include <quanser.h>

typedef struct lista
{
	double value;
	long timestamp;
	struct lista *next;
}lista;

typedef struct pid_struct
{
	double error;		// Erro anterior na medida
	double integral;	// Acumuladora da integral
	long time;
	double Kc;		// Parâmetro do produto
	double Ti;		// Parâmetro da integral
	double Td;		// Parâmetro da divisão
	double Tc;		// Cte de tempo do loop
	double CO;		// Output do controlador (pwm?)
	double COb;		// Bias do output
	double PV;		// Variável do processo (leitura do sensor)
	double Kp;		// Ganho do processo, usado no modelo
	double Tp;		// Cte de tempo do processo, usado no modelo
	double Thetap;		// Dead-time
	double SP;		// Set point
}pid_struct;

extern lista* amostras;
extern pid_struct* PID_struct;

//Init functions
void initPID();
void initSamples(lista* novo);

//Sample functions
void cleanSamples();
void addSample(long time, double value);
lista* getLastSample();
lista* getNewSample(long timestamp, QUANSER quanser);
void createRandomSamples();

//Tests
void bumpTest(QUANSER quanser);
void bumplessTransfer(QUANSER quanser);
void monitorBump(long *startTime, double *currentPV, QUANSER quanser);

//Utilities
long getTimeAt(double currentPV);

//PID calcul
double calculatePID(QUANSER quanser);
#endif
